/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenc1;

/**
 *
 * @author Administrator
 */
public class Contrato {
    private int claveC;
    private String puesto;
    private float ImpuestoISR;

    public Contrato() {
        this.claveC=0;
        this.puesto="";
        this.ImpuestoISR=0.0f;
    }

    public Contrato(int claveC, String puesto, float ImpuestoISR) {
        this.claveC = claveC;
        this.puesto = puesto;
        this.ImpuestoISR = ImpuestoISR;
    }

    public int getClaveC() {
        return claveC;
    }

    public void setClaveC(int claveC) {
        this.claveC = claveC;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoISR() {
        return ImpuestoISR;
    }

    public void setImpuestoISR(float ImpuestoISR) {
        this.ImpuestoISR = ImpuestoISR;
    }
    
    
    
}
