/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenc1;

/**
 *
 * @author Administrator
 */
public class EmpleadoD extends Empleados{
    private int nivel;
    private float horasLab;
    private float pagoHora;

    public EmpleadoD() {
        this.nivel=0;
        this.horasLab=0.0f;
        this.pagoHora=0.0f;
    }

    public EmpleadoD(int nivel, float horasLab, float pagoHora, int numEmpleado, String nombre, String domicilio, Contrato contrato) {
        super(numEmpleado, nombre, domicilio, contrato);
        this.nivel = nivel;
        this.horasLab = horasLab;
        this.pagoHora = pagoHora;
    }

    

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getHorasLab() {
        return horasLab;
    }

    public void setHorasLab(float horasLab) {
        this.horasLab = horasLab;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }
    
    @Override
    public float calcularTotal(int t) {
       float f=0;
       f=this.pagoHora*this.horasLab;
       if(t==1)
           f=f + (f*0.35f);
       if(t==2)
           f=f + (f*0.40f);
       if(t==3)
           f=f + (f*0.50f);
       else
           return f;
      return f;
    }

    @Override
    public float calcularImpuesto() {
        return (this.pagoHora*this.horasLab) * (this.contrato.getImpuestoISR()/100);
    }
    
}
